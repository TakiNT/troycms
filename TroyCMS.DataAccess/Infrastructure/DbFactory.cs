﻿namespace TroyCMS.DataAccess.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        TroyCMSContext dbContext;

        public TroyCMSContext Init()
        {
            return dbContext ?? (dbContext = new TroyCMSContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
