﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.DataAccess.Models;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Services
{
    public interface ICommentService : IService<Comment>
    {
        Task<IEnumerable<GetCommentDTO>> GetComments(int skip, int take, ActionFor actionFor, int actionForId);
        Task<GetCommentDTO> CreateComment(NewCommentDTO newComment, string userName);
    }
}
