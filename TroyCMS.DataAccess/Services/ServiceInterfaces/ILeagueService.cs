﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.DataAccess.Models;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Services
{
    public interface ILeagueService : IService<League>
    {
        IEnumerable<GetLeagueDTO> GetLeagues(int skip, int take, string userName, ref int count);
        Task<GetLeagueDTO> CreateOrUpdateLeague(NewLeagueDTO newLeague, string userName);
    }
}
