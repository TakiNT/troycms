﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.DataAccess.Models;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Services
{
    public interface ILikeService : IService<Like>
    {
        Task<IEnumerable<GetLikeDTO>> GetLikes(int skip, int take, ActionFor actionFor, int actionForId);
        Task<GetLikeDTO> ToggleLike(NewLikeDTO newLike, string userName);
    }
}
