﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Models
{
    public class NewLikeDTO
    {
        public int Id { get; set; }
        public int ActionForId { get; set; }
        public ActionFor ActionFor { get; set; }
    }
}