﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TroyCMS.Entities.Models
{
    public interface IObjectState
    {
        [NotMapped]
        ObjectState ObjectState { get; set; }
    }

    public enum ObjectState
    {
        Unchanged,
        Added,
        Modified,
        Deleted
    }
}
