namespace TroyCMS.Entities.Models
{
    public enum TagType
    {
        League, Team, Player
    }
}