﻿namespace TroyCMS.Entities.Models.Enums
{
    public enum Gender
    {
        Male, Female, Others
    }
}
