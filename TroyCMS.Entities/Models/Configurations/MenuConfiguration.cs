﻿using System.Data.Entity.ModelConfiguration;

namespace TroyCMS.Entities.Models.Configurations
{
    public class MenuConfiguration : EntityTypeConfiguration<Menu>
    {
        public MenuConfiguration()
        {
            HasOptional(e => e.Parent)
            .WithMany()
            .HasForeignKey(m => m.ParentId);
        }
    }

}
